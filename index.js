// console.log("Congrats, Batch 243!");


// Section: Document Object Model (DOM)

	// allows us to access or modify the properties of an html element in a webpage
	// it is standard on how to get,change,add, or delete HTML elements
	// we will focus on use of DOM in managing forms. 
	
//for selecting HTML elements we will be using document.querySelector
	 //Syntax:document.querySelector("html element")
	 //the quesrySelector function takes a string input that is formatted like a css selector when applying the styles 

const txtFirstName = document.querySelector("#txt-first-name");
// console.log(txtFirstName);
const txtLastName = document.querySelector("#txt-last-name");

const spanFullName = document.querySelector("#fullName");

const name=document.querySelectorAll(".full-name");
// console.log(name);

const span=document.querySelectorAll("span");
// console.log(span);

const text =document.querySelectorAll("input[type]");
// console.log(text);


// Section:Event Listeners
	// whenever a user interacts with a webpage,this action is considered as an event.
	// working with events is large part of creating interactivity in a webpage.
	//specific functions that perform an action 

// The function use is "addEventListener,it takes two arguments"
	//first argument a string identifying a event.
	// second argument,function that the listener will trigger once the "specified event" is triggered.

/*txtFirstName.addEventListener('keydown',(event)=>{
	console.log(event.target.value);
})*/

const fullName=()=>{
	spanFullName.innerHTML=`${txtFirstName.value} ${txtLastName.value}`
}
txtFirstName.addEventListener("keyup", fullName);
txtLastName.addEventListener("keyup", fullName);



/*const green = document.querySelector("#green");
const blue = document.querySelector("#blue");
const red = document.querySelector("#red");

btn.addEventListener('click', function onClick(event) {
  // 👇️ Change text color globally
  document.body.style.color = 'darkgreen';

  // 👇️ Change text color for clicked element only
  // event.target.style.color = 'salmon';
});*/


/*const btn = document.getElementById('btn');

btn.addEventListener('click', function change(event) {
  const box = document.getElementById('box');

  green.style.color = 'green';
  blue.style.color = 'blue';
  red.style.color = 'red';
});
*/
const color = document.querySelector("#color");

const colorChange=()=>{
	spanFullName.style.color = `${color.value}`;
}

color.addEventListener("click", colorChange);





